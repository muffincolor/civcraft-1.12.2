package net.hnm.rp.commands;

import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpyCommand implements CommandExecutor {
    public static ArrayList<Player> spy = new ArrayList();

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Команда может быть выполнена только игроком");
            return true;
        } else {
            Player p = (Player)sender;
            if (!p.hasPermission("PhoenixFaction.pfspy")) {
                p.sendMessage(ChatColor.RED + "У вас нет прав включать шпион чатов фракций");
                return true;
            } else {
                if (spy.contains(p)) {
                    spy.remove(p);
                    p.sendMessage(ChatColor.GREEN + "Шпион чатов фракций выключен");
                } else {
                    spy.add(p);
                    p.sendMessage(ChatColor.GREEN + "Шпион чатов фракций включен");
                }

                return true;
            }
        }
    }
}