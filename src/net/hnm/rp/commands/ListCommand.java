package net.hnm.rp.commands;

import java.util.Iterator;
import net.hnm.rp.main.HeavenRolePlay;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import java.util.ArrayList;

public class ListCommand implements CommandExecutor {
    private HeavenRolePlay plugin;

    public ListCommand(HeavenRolePlay plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Команда может быть выполнена только игроком");
            return true;
        } else {
            Player p = (Player)sender;
            if (!this.plugin.getData().contains("players." + p.getName().toLowerCase())) {
                sender.sendMessage(ChatColor.RED + "Вы не состоите в какой-либо фракции");
                return true;
            } else {
                int faction = Integer.parseInt(this.plugin.getData().getString("players." + p.getName().toLowerCase()).split(" ")[0]);
                ArrayList<Player> players = new ArrayList();
                Iterator var9 = Bukkit.getOnlinePlayers().iterator();

                while(var9.hasNext()) {
                    Player player = (Player)var9.next();
                    if (this.plugin.getData().contains("players." + player.getName().toLowerCase()) && Integer.parseInt(this.plugin.getData().getString("players." + player.getName().toLowerCase()).split(" ")[0]) == faction) {
                        players.add(player);
                    }
                }

                int j;
                for(j = 0; j < players.size(); j += 9) {
                    ;
                }

                Inventory i = Bukkit.createInventory((InventoryHolder)null, j, ChatColor.LIGHT_PURPLE + "Список игроков в вашей фракции");
                j = 0;

                for(Iterator var11 = players.iterator(); var11.hasNext(); ++j) {
                    Player player = (Player)var11.next();
                    ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1);
                    SkullMeta meta = (SkullMeta)skull.getItemMeta();
                    meta.setOwner(player.getName());
                    meta.setDisplayName(ChatColor.AQUA + player.getName());
                    ArrayList<String> lores = new ArrayList();
                    lores.add(ChatColor.GREEN + "Номер ранга: " + this.plugin.getData().getString("players." + player.getName().toLowerCase()).split(" ")[1]);
                    meta.setLore(lores);
                    skull.setItemMeta(meta);
                    i.setItem(j, skull);
                }

                p.openInventory(i);
                return true;
            }
        }
    }
}
