package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class SEPFaction {
    public static String FactionName = "Сепаратисты";
    public static ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public SEPFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("1-ый ранг (Сын)");
        ranks.add("2-ой ранг (Сын)");
        ranks.add("3-ий ранг (Сын)");
        ranks.add("4-ый ранг (Сын)");
        ranks.add("5-ый ранг (Сын)");
        ranks.add("6-ой ранг (Сын)");
        ranks.add("Заместитель отряда Getting");
        ranks.add("Заместитель отряда Rush");
        ranks.add("Глава отряда Getting");
        ranks.add("Глава отряда Rush");
        ranks.add("Брат отца");
        ranks.add("Отец");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "8 0");
        plugin.saveDataConfig();
    }
}
