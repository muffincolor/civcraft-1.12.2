package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class MCHSFaction {
    public static String FactionName = "МЧС";
    public static ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public MCHSFaction(HeavenRolePlay plugin) {
        plugin = plugin;
        ranks.add("Ученик");
        ranks.add("Диспетчер");
        ranks.add("Пожарный");
        ranks.add("Водитель");
        ranks.add("Старший пожарный");
        ranks.add("Старший водитель");
        ranks.add("Командир отделения");
        ranks.add("Заместитель начальника части");
        ranks.add("Начальник части");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "7 0");
        plugin.saveDataConfig();
    }
}
