package net.hnm.rp.factions;

import java.util.ArrayList;
import net.hnm.rp.main.HeavenRolePlay;

public class AOFaction
{
    public static final String FactionName = "АО";
    public static final ArrayList<String> ranks = new ArrayList();
    private static HeavenRolePlay plugin;

    public AOFaction(HeavenRolePlay plugin) {
        AOFaction.plugin = plugin;
        ranks.add("Водитель");
        ranks.add("Охранник");
        ranks.add("Начальник охраны");
        ranks.add("Секретарь");
        ranks.add("Старший секретарь");
        ranks.add("Адвокат");
        ranks.add("Прокурор");
        ranks.add("Мэр г.Подольск");
        ranks.add("Мэр г.Чехов");
        ranks.add("Президент РФ");
    }

    public static void invite(String name) {
        plugin.getData().set("players." + name, "2 0");
        plugin.saveDataConfig();
    }
}
